import { browser } from "protractor";

// Require protractor-beautiful-reporter to generate reports.
var HtmlReporter = require("protractor-beautiful-reporter");
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
exports.config = {
  directConnect: true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: "chrome",
    acceptInsecureCerts : true,
    chromeOptions: {
      args: [
        "--disable-gpu",
        "--disable-extension",
        "--start-maximized",
        "--allow-running-insecure-content",
        "--disable-web-security",
        "--allow-file-access",
        "--allow-insecure-localhost",
        "--ignore-certificate-errors"
    ]
    }
  },

  // If you have one app to test then you can mention the base url here.

  // Framework to use. Jasmine is recommended.
  framework: "jasmine2",

  specs: ["../temp/test-suites/address/*.js"],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 90000,
    isVerbose: true
  },

  onPrepare: () => {
    browser
      .manage()
      .window()
      .maximize();
    browser
      .manage()
      .timeouts()
      .implicitlyWait(30000);

    // Add a screenshot reporter and store screenshots to `./test-results`:
    jasmine.getEnv().addReporter(
      new HtmlReporter({
        baseDirectory: "test-results",
        preserveDirectory: false, // Preserve base directory
        screenshotsSubfolder: "screenshots",
        jsonsSubfolder: "jsons", // JSONs Subfolder
        takeScreenShotsForSkippedSpecs: true, // Screenshots for skipped test cases
        takeScreenShotsOnlyForFailedSpecs: false, // Screenshots only for failed test cases
        docTitle: "Coyo Test Automation Execution Report", // Add title for the html report
        docName: "Coyo_TestResult.html", // Change html report file name
        gatherBrowserLogs: true // Store Browser logs
      }).getJasmine2Reporter()
    );
  }
};
