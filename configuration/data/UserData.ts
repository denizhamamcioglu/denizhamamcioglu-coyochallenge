import { DataGenerator } from "../../helpers/DataGenerator";

export class UserData {
  static validUser = {
    email: DataGenerator.generateRandomString(4) + "@mail.com",
    password: DataGenerator.generateRandomString(5)
  };
}
