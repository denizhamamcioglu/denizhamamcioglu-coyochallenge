import { DataGenerator } from "../../helpers/DataGenerator";

export class AddressData {
  static validAddress = {
    firstName: DataGenerator.generateRandomString(4, "autoCandidateFirst_"),
    lastName: DataGenerator.generateRandomString(4, "autoCandidateLast_"),
    address1: DataGenerator.generateRandomString(4, "autoAddress1_"),
    address2: DataGenerator.generateRandomString(4, "autoAddress2_"),
    city: DataGenerator.generateRandomString(4, "autoCity_"),
    zip: "11111",
    birthdate: DataGenerator.generatePastDate(20, "year", "MM/DD/YYYY"),
    age: DataGenerator.generateRandomNumber(2),
    website: "https://www." + DataGenerator.generateRandomString(4) + ".com",
    phone: DataGenerator.generateRandomNumber(10),
    state: DataGenerator.generateRandomNumber(1)
  };
}
