import {
  browser,
  by,
  element,
  Constants,
  ElementFinder,
  ExpectedConditions
} from ".";

/**
 * @classdesc Contains methods that can be used as helpers accross all tests.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class Utils {
  // Waits
  /**
   * @method waitForElementVisible - Waits for the targetElement to be visible on the page.
   * @param {ElementFinder} targetElement - Element that will be waited to be visible.
   */
  static async waitForElementVisible(targetElement: ElementFinder) {
    await browser.wait(
      ExpectedConditions.visibilityOf(targetElement),
      Constants.TIMEOUT
    );
  }

  /**
   * @method waitForElementPresent - Waits for the targetElement to be present on the page.
   * @param {ElementFinde} targetElement - Element that will be waited to be present.
   */
  static async waitForElementPresent(targetElement: ElementFinder) {
    await browser.wait(
      await ExpectedConditions.presenceOf(targetElement),
      Constants.TIMEOUT
    );
  }

  /**
   * @method waitForElementToBeClickable - Waits for the targetElement to be clickable on the page.
   * @param {ElementFinder} targetElement - Element that will be waited to be clickable.
   */
  static async waitForElementToBeClickable(targetElement: ElementFinder) {
    await browser.wait(
      ExpectedConditions.elementToBeClickable(targetElement),
      Constants.TIMEOUT
    );
  }

  /**
   * @method waitForElementToBeSelected - Waits for the targetElement to be selected on the page.
   * @param {ElementFinder} targetElement - Element that will be waited to be selected.
   */
  static async waitForElementToBeSelected(targetElement: ElementFinder) {
    await browser.wait(
      ExpectedConditions.elementToBeSelected(targetElement),
      Constants.TIMEOUT
    );
  }

  /**
   * @method waitForElementInvisible - Waits for the targetElement to be invisible on the page.
   * @param {ElementFinder} targetElement - Element that will be waited to be invisible.
   */
  static async waitForElementInvisible(targetElement: ElementFinder) {
    await browser.wait(
      ExpectedConditions.invisibilityOf(targetElement),
      Constants.TIMEOUT
    );
  }

  /**
   * @method waitForElementNotPresent - Waits for the targetElement to be not present on the page.
   * @param {ElementFinder} targetElement - Element that will be waited to be not present.
   */
  static async waitForElementNotPresent(targetElement: ElementFinder) {
    await browser.wait(
      ExpectedConditions.stalenessOf(targetElement),
      Constants.TIMEOUT
    );
  }

  /**
   * @method waitForTitleContains - Waits until the title contains the given text.
   * @param {string} text - String to be searched in the title of the page.
   */
  static async waitForTitleContains(text: string) {
    await browser.wait(
      ExpectedConditions.titleContains(text),
      Constants.TIMEOUT
    );
  }

  /**
   * @method waitForTitleIs - Waits until the title is equals to the given text.
   * @param {string} text - String to be checked with the page title.
   */
  static async waitForTitleIs(text: string) {
    await browser.wait(ExpectedConditions.titleIs(text), Constants.TIMEOUT);
  }

  /**
   * @method waitForUrlContains - Waits until the page URL contains the given text.
   * @param {string} text - String to be searched in the URL of the page.
   */
  static async waitForUrlContains(text: string) {
    await browser.wait(ExpectedConditions.urlContains(text), Constants.TIMEOUT);
  }

  /**
   * @method waitForUrlIs - Waits until the page URL is equals to the given text.
   * @param {string} text - String to be checked with the page URL.
   */
  static async waitForUrlIs(text: string) {
    await browser.wait(ExpectedConditions.urlIs(text), Constants.TIMEOUT);
  }

  /**
   * @method waitForElementContainsText - Waits until the targetElement contains the given text.
   * @param {ElementFinder} targetElement - Element that will be waited until it contains the given text.
   * @param {string} text - Text to be searched within the target element.
   */
  static async waitForElementContainsText(
    targetElement: ElementFinder,
    text: string
  ) {
    await browser.wait(
      ExpectedConditions.textToBePresentInElement(targetElement, text)
    );
  }

  /**
   * @method waitFor - Waits for the given amount of time.
   * @param {number} timeout - Waiting time, given in milliseconds.
   */
  static async waitFor(timeout: number) {
    await browser.sleep(timeout);
  }

  // Selects
  /**
   * @method selectByDropdownValue - Selects the option with the given value from the target dropdown element.
   * @param {ElementFinder} targetElement - Target dropdown element.
   * @param {string} value - Value to be searched among the dropdown options.
   * @param {string} itemHtmlAttribute - Attribute type to be controlled on the targetElement. Some dropdowns list items by using "option" tags, some use "span" tags.
   * For future proofing the method, HTML tag / attribute names are parameterized.
   */
  static async selectByDropdownValue(
    targetElement: ElementFinder,
    value: string,
    itemHtmlAttribute: string
  ) {
    await targetElement
      .element(by.cssContainingText(itemHtmlAttribute, value))
      .click();
  }

  /**
   * @method selectByDropdownIndex - Selects the option with the given index from the target dropdown element.
   * @param {ElementFinder} targetElement - Target dropdown element.
   * @param {any} index - Index of the to be selected option of the target dropdown element.
   */
  static async selectByDropdownIndex(targetElement: ElementFinder, index: any) {
    await targetElement
      .element(await by.css("option:nth-child(" + index + ")"))
      .click();
  }

  // Getters
  /**
   * @method getFromTableByRow - Returns the text of the given row from the target table.
   * @param {ElementFinder} targetElement - Target table element.
   * @param {number} row - Row index of the target table element.
   * @returns {string} returnedText - Text value of the target table's target row.
   */
  static async getFromTableByRow(targetElement: ElementFinder, row: number) {
    // col variable is redundant at this point. Since there is no "td" tag on the membership list table, column number is not used.
    const rowText = await targetElement
      .all(by.tagName("tr"))
      .get(row)
      .getText();
    return rowText;
  }

  /**
   * @method getFromTableByIndex - Returns the text of the given row and column from the target table.
   * @param {ElementFinder} targetElement - Target table element.
   * @param {number} row - Row index of the target table element.
   * @param {number} col - Column index of the target table element.
   * @returns {string} returnedText - Text value of the target table's target row's target cell.
   */
  static async getFromTableByIndex(
    targetElement: ElementFinder,
    row: number,
    col: number
  ) {
    
    const cellText = await targetElement.all(by.tagName("tr")).get(row).all(by.tagName("td")).get(col).getText();
    return cellText;
  }

  /**
   * @method clickTableRow - Clicks the nth row of a table.
   * @param {ElementFinder} targetElement - Target table element.
   * @param {number} row - Row index of the target table element.
   */
  static async clickTableCell(targetElement: ElementFinder, row: number, col: number) {
    const targetRow: any = await targetElement
      .all(by.tagName("tr"))
      .get(row)
      .all(by.tagName("td"))
      .get(col)
      .click();
  }

  /**
   * @method getTableLength - Returns the row count of the given table.
   * @param {ElementFinder} targetElement - Target table element.
   * @returns {number} tableLength - Row count of the target table element.
   */
  static async getTableLength(targetElement: ElementFinder) {
    const tableLength: any = await targetElement.all(by.tagName("tr")).count();
    return tableLength;
  }

  /**
   * @method getAllWindowHandles - Returns currently active browser windows.
   * @returns {Array<string>} windowHandles - List of currently active browser windows.
   */
  static async getAllWindowHandles() {
    const windowHandles: any = await browser.driver.getAllWindowHandles();
    return windowHandles;
  }

  static async getActivityTable(
    targetElement: ElementFinder,
    row: number,
    column?: any
  ) {
    if (!column) {
      return await targetElement
        .all(by.tagName("tbody tr:not(.spacer)"))
        .get(row)
        .getText();
    }
    return await targetElement
      .all(by.tagName("tbody tr:not(.spacer)"))
      .get(row)
      .all(by.tagName("td"))
      .get(column)
      .getText();
  }

  static async clickActivityTable(targetElement: ElementFinder, row) {
    await element
      .all(by.className("clickable animate ng-star-inserted"))
      .get(row)
      .click();
  }

  /**
   * @method getWindowHandle - Returns the desired window handle instance.
   * @param {number} index - Window handle index.
   * @returns {Array<string>} windowHandle - List of currently active browser windows.
   */
  static async getWindowHandle(index: number) {
    const windowHandles: any = await browser.driver.getAllWindowHandles();
    const desiredWindowHandle: any = await windowHandles[index];
    return desiredWindowHandle;
  }

  // Navigation
  /**
   * @method goToMember - INCOMPLETE - Navigates to the given member's profile at the given environment (PDT, UAT, DEV or local)
   * @param {number} memberId - Membership ID of the target member.
   * @param {string} targetEnvironment - Target environment.
   */
  static async goToMember(memberId: string, targetEnvironment: string) {
    // TODO: complete.
  }

  // Scrolls
  /**
   * @method scrollToCoordinates - Performs scroll operation until the page reaches the given coordinates.
   * @param {number} x - X coordinate.
   * @param {number} y - Y coordinate.
   */
  static async scrollToCoordinates(x: number, y: number) {
    await browser.executeScript(`window.scrollTo(${x}, ${y})`);
  }

  /**
   * @method scrollToElement - Performs scroll operation until the target element is in view.
   * @param {ElementFinder} targetElement - Target element to be scrolled into.
   */
  static async scrollToElement(targetElement: ElementFinder) {
    await browser
      .actions()
      .mouseMove(targetElement)
      .perform();
  }

  /**
   * @method scrollToBottom - Performs scroll operation until the bottom of the page is reached.
   */
  static async scrollToBottom() {
    await browser.executeScript(
      "window.scrollTo(0, document.body.scrollHeight)"
    );
  }

  /**
   * @method generateParsedInputDate - Takes a date input from api response and parse it to ui standarts.
   * @param {string} inputDate - Input date in string.
   */

  static generateParsedInputDate(inputDate: string) {
    const date: any = new Date(inputDate);
    const exactMonth: any = date.getMonth() + 1;
    const mothWithZero: any = ("0" + exactMonth).slice(-2);
    const dayWithZero: any = ("0" + date.getDate()).slice(-2);
    const year: any = date.getFullYear();
    const backSlash: any = "\uE029";

    const parsedDate: any =
      dayWithZero + backSlash + mothWithZero + backSlash + year;

    return parsedDate;
  }

  static async hoverOverElement(targetElement: ElementFinder) {
    await browser
      .actions()
      .mouseMove(targetElement)
      .perform();
  }
  // Checks
}
