const fs = require('fs');

/**
 * @classdesc Helper class that is used for JSON parsing.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class JSONParser {
  /**
   * @method parseJson - Reads the JSON file from the given file path and returns it as a JSON object.
   * @param {string} filePath - File path for the target JSON file.
   * @returns {JSON} parsedJsonData - Parsed JSON object.
   */
  static parseJson(filePath: string) {
    const rawJsonData: any = fs.readFileSync(filePath);
    const parsedJsonData: any = JSON.parse(rawJsonData);
    return parsedJsonData;
  }
}
