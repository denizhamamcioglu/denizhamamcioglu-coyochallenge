import { moment, momentRandom } from '.';

/**
 * @namespace DataGenerator - Used for generating different types of data.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class DataGenerator {
  /**
   * @method generateRandomString - Generates a random string with the given prefix and length
   * @param {string} prefix Optional - String prefix that will be prepended to the generated random string.
   * @param {number} length - Length of the generated random string.
   * @returns {string} - Random string with the given length and prefix.
   */
  static generateRandomString(length: number, prefix?: string) {
    if (prefix === undefined) {
      prefix = '';
    } else if (prefix.length === 0) {
      prefix = '';
    }

    let text: any = '';
    const possible: any = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return prefix + text;
  }

  /**
   * @method generateRandomCapitalStringWithNumbers - Generates a random capital letter string with numbers.
   * @param length - Length of the random string that will be generated.
   */
  static generateRandomCapitalStringWithNumbers(length: number) {
    let generatedString: any = '';
    const possible: any = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    for (let i = 0; i < length; i++) {
      generatedString += possible.charAt(
        Math.floor(Math.random() * possible.length)
      );
    }

    return generatedString;
  }

  /**
   * @method generateRandomNumber - Generates a random number with the given range and length
   * @param length - Length of the random number that will be generated.
   * @returns {string} - Generated random number with the given range and length.
   */
  static generateRandomNumber(length) {
    let generatedNumber: any = '';
    const possible: any = '0123456789';
    for (let i = 0; i < length; i++) {
      generatedNumber += possible.charAt(
        Math.floor(Math.random() * possible.length)
      );
    }
    
    return generatedNumber;
  }

  /**
   * @method generateRandomDate - Generates a random date.
   * @returns {Date} - Generated random date.
   */
  static generateRandomDate(format: string) {
    return momentRandom().format(format); // L
  }

  /**
   * @method generateTimestamp - Generates a timestamp in milliseconds.
   * @returns {Date} - Timestamp in milliseconds.
   */
  static generateTimestamp() {
    return Date.now() / 1000;
  }

  /**
   * @method getTodaysDate - Returns the today's date in MM/DD/YYYY format.
   * @param {string} format - Sets the formatting according to this value.
   * @returns {Date} - Todays date with the MM/DD/YYYY format.
   */
  static getTodaysDate(format: string) {
    return moment().format(format);
  }

  /**
   * @method getMeaningfulTimestamp - Returns a readable timestamp in the format of DayMonthYear_hourMinute
   * @param {boolean} withYear - Current year will be included in the timestamp if this is set as true.
   * @returns {Date} - Current readable time stamp.
   */
  static getMeaningfulTimestamp(withYear: boolean) {
    if (withYear) {
      return moment().format('DDMMYYYY_hhmm');
    }
    return moment().format('DDMM_hhmm');
  }

  /**
   * @method generatePastDate - Generates a random date that is earlier than today with the given amount.
   * @param {number} amount - The number of time units (days, months, years) that will be substracted from today's date.
   * @param {string} type - Time unit type (days, months, years).
   * @param {boolean} format - Sets the formatting according to the provided string.
   * @returns {Date} - Generated random that that is specified number of time units earlier than today's date.
   */
  static generatePastDate(
    amount: number,
    type: string,
    format: string
  ) {
    return moment()
      .subtract(amount, type)
      .format(format);
  }

  /**
   * @method generateFutureDate - Generates a date that is specified number of time units later than today's date.
   * @param {number} amount - The number of time units (days, months, years) that will be added to today's date.
   * @param {string} type - Time unit type (days, months, years).
   * @param {string} format - Sets the formatting according to the provided string.
   * @returns {Date} - Generated random that that is specified number of time units later than today's date.
   */
  static generateFutureDate(
    amount: number,
    type: string,
    format: string
  ) {
    return moment()
      .add(amount, type)
      .format(format); // use 'L' for the old format. DD\uE029MM\uE029YYYY
  }

  /**
   * @method generateDate - Generates a future or past date depends on todays date
   * @param {boolean} firstDayOfMonth - Returns the first day of the month pf result.
   * @param {string} format - Sets the formatting according to the provided string.
   * @param {number} dayDiff - Day count that needs to be add or substract.
   * @param {number} monthDiff - Month count that needs to be add or substract.
   * @param {number} yearDiff - Year count that needs to be add or substract.
   * @returns {Date} - Generated random that that is specified number of time units later than today's date.
   */

  static generateDate(
    firstDayOfMonth: boolean,
    format: string,
    dayDiff: number,
    monthDiff?: number,
    yearDiff?: number
  ) {
    monthDiff = monthDiff || 0;
    yearDiff = yearDiff || 0;

    if (firstDayOfMonth) {
      return moment()
        .add(dayDiff, 'd')
        .add(monthDiff, 'M')
        .add(yearDiff, 'y')
        .startOf('month')
        .format(format);
    }
    {
      return moment()
        .add(dayDiff, 'd')
        .add(monthDiff, 'M')
        .add(yearDiff, 'y')
        .format(format);
    }
  }
}
