import { Utils, LandingSelectors, LandingActions } from ".";

/**
 * @classdesc Landing page checks.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class LandingChecks {
  protected selectors = new LandingSelectors();
  protected actions = new LandingActions();

}
