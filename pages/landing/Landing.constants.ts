/**
 * @classdesc Landing page constants.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class LandingConstants {

    static get content() {
        return {
            URL: 'http://a.testaddressbook.com/', // TODO: make this dynamic according to the browser.params.environment parameter
            pageTitle: 'Address Book',
        };
    }


}
