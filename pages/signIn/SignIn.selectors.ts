import { by, element } from ".";

/**
 * @classdesc Sign In page selectors.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */

export class SignInSelectors {
  // Input Fields
  emailField = element(by.id("session_email"));
  passwordField = element(by.id("session_password"));

  // Checkboxes

  // Buttons
  signInButton = element(by.css("[value='Sign in']"));
  signUpButton = element(by.linkText("Sign up"));

  // Dropdowns

  // Labels & Text & Links
  
}
