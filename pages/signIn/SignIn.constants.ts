/**
 * @classdesc Sign In page constants.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class SignInConstants {
  static get content() {
    return {
      URL: "http://a.testaddressbook.com/sign_in", // TODO: make this dynamic according to the browser.params.environment parameter
      pageTitle: "Address Book - Sign In"
    };
  }
}
