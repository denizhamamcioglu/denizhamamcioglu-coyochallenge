import { SignInSelectors, SignInActions, ElementFinder } from ".";

/**
 * @classdesc Sign In page checks.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class SignInChecks {
    protected selectors = new SignInSelectors();
    protected actions = new SignInActions();

}