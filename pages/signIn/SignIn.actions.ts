import { JSONParser, browser,  Key } from ".";
import { SignInSelectors } from "./SignIn.selectors";

/**
 * @classdesc Sign In page actions.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class SignInActions {
    protected selectors = new SignInSelectors();
    
    // Field actions
   async enterEmail(email: string) {
       await this.selectors.emailField.sendKeys(email);
   }

   async enterPassword(pass: string) {
       await this.selectors.passwordField.sendKeys(pass);
   }

    // Clicks
    async clickSignIn() {
        await this.selectors.signInButton.click();
    }

    async clickSignUp() {
        await this.selectors.signUpButton.click();
    }
    
    // Helpers
    async login(email: string, password: string) {
        await this.enterEmail(email);
        await this.enterPassword(password);
        await this.clickSignIn();
    }
}