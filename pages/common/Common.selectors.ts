import { by, element } from ".";

/**
 * @classdesc Common page selectors.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */

export class CommonSelectors {
  // Input Fields

  // Checkboxes

  // Buttons

  // Dropdowns

  // Labels & Text & Links
  homeLink = element(by.linkText("Home"));
  signInLink = element(by.linkText("Sign in"));
  addressesLink = element(by.linkText("Addresses"));
  signOutLink = element(by.linkText("Sign out"));
  currentUserLabel = element(by.css("[data-test='current-user']"));
}
