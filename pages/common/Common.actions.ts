import { JSONParser, browser,  Key } from ".";
import { CommonSelectors } from "./Common.selectors";

/**
 * @classdesc Common page actions.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class CommonActions {
    protected selectors = new CommonSelectors();
    
    // Field actions
   
    // Clicks
    async clickSignIn() {
        await this.selectors.signInLink.click();
    }

    async clickHome() {
        await this.selectors.homeLink.click();
    }
    
    async clickAddresses() {
        await this.selectors.addressesLink.click();
    }

    async clickSignOut() {
        await this.selectors.signOutLink.click();
    }

    // Getters
    async getCurrentUser() {
        return await this.selectors.currentUserLabel.getText();
    }
    
    // Helpers
    
}