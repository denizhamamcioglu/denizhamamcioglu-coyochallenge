import { CommonSelectors, CommonActions, ElementFinder } from ".";

/**
 * @classdesc Common page checks.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class CommonChecks {
    protected selectors = new CommonSelectors();
    protected actions = new CommonActions();
    
}