import { Key, CommonActions, AddressSelectors, Utils, AddressData } from ".";

/**
 * @classdesc Address page actions.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class AddressActions {
  protected selectors = new AddressSelectors();
  protected commonActions = new CommonActions();
  
  // Field actions
  async enterFirstName(firstName: string) {
    await this.selectors.firstNameField.sendKeys(firstName);
  }
  
  async enterLastName(lastName: string) {
    await this.selectors.lastNameField.sendKeys(lastName);
  }

  async enterAddress1(add1: string) {
    await this.selectors.address1Field.sendKeys(add1);
  }

  async enterAddress2(add2: string) {
    await this.selectors.address2Field.sendKeys(add2);
  }

  async enterCity(city: string) {
    await this.selectors.cityField.sendKeys(city);
  }

  async enterZipCode(zip: string) {
    await this.selectors.zipCodeField.sendKeys(zip);
  }

  async enterBirthday(birthday: string) {
    await this.selectors.birthdayField.sendKeys(birthday);
  }

  async enterAge(age: string) {
    await this.selectors.ageField.sendKeys(age);
  }

  async enterWebsite(website: string) {
    await this.selectors.websiteField.sendKeys(website);
  }

  async enterPhone(phone: string) {
    await this.selectors.phoneField.sendKeys(phone);
  }

  async enternote(note: string) {
    await this.selectors.noteField.sendKeys(note);
  }

  // Selects
  async selectStateByValue(state: string) {
    await Utils.selectByDropdownValue(this.selectors.stateDropdown, state, "option");
  }

  async selectStateByIndex(index: number) {
    await Utils.selectByDropdownIndex(this.selectors.stateDropdown, index);
  }

  async selectCountry(country: string) {
    if (country == "canada") {
      await this.selectors.canadaRadio.click();
    } else {
      await this.selectors.usRadio.click();
    }
  }

  async selectClimbing() {
    await this.selectors.climbingChecbox.click();
  }

  async selectDancing() {
    await this.selectors.dancingCheckbox.click();
  }

  async selectReading() {
    await this.selectors.readingCheckbox.click();
  }

  // Clicks
  async clickCreateAddress() {
    await this.selectors.createAddressButton.click();
  }

  async clickList() {
    await this.selectors.listButton.click();
  }

  async clickNewAddress() {
    await this.selectors.newAddressButton.click();
  }

  // Helpers
  async createNewAddress() {
    await this.enterFirstName(AddressData.validAddress.firstName);
    await this.enterLastName(AddressData.validAddress.lastName);
    await this.enterAddress1(AddressData.validAddress.address1);
    await this.enterAddress2(AddressData.validAddress.address2);
    await this.enterCity(AddressData.validAddress.city);
    await this.selectStateByIndex(AddressData.validAddress.state);
    await this.enterZipCode(AddressData.validAddress.zip);
    await this.selectCountry("canada");
    await this.enterBirthday(AddressData.validAddress.birthdate);
    await this.enterAge(AddressData.validAddress.age);
    await this.enterWebsite(AddressData.validAddress.website);
    await this.enterPhone(AddressData.validAddress.phone);
    await this.selectClimbing();
    await this.selectDancing();
    await this.selectReading();
    await this.clickCreateAddress();
  }
}
