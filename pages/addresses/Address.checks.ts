import { Utils, AddressSelectors, AddressActions } from ".";

/**
 * @classdesc Candidate page checks.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class AddressChecks {
  protected selectors = new AddressSelectors();
  

  async isAddressCreated() {
    expect(await this.selectors.addressNoticeLabel.getText()).toContain("Address was successfully created.")
  }

}
