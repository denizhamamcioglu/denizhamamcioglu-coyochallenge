/**
 * @classdesc Address page constants.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class AddressConstants {

    static get content() {
        return {
            URL: 'http://a.testaddressbook.com/addresses', // TODO: make this dynamic according to the browser.params.environment parameter
            pageTitle: 'Address Book',
        };
    }


}
