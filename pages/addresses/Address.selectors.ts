import { by, element } from ".";

/**
 * @classdesc Address page selectors.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class AddressSelectors {
  // Input Fields
  newAddressButton = element(by.css("[data-test='create']"));
  firstNameField = element(by.id("address_first_name"));
  lastNameField = element(by.id("address_last_name"));
  address1Field = element(by.id("address_street_address"));
  address2Field = element(by.id("address_secondary_address"));
  cityField = element(by.id("address_city"));
  zipCodeField = element(by.id("address_zip_code"));
  ageField = element(by.id("address_age"));
  websiteField = element(by.id("address_website"));
  noteField = element(by.id("address_note"));
  birthdayField = element(by.id("address_birthday"));
  phoneField = element(by.id("address_phone"));
  
  // Checkboxes & Switches
  usRadio = element(by.id("address_country_us"));
  canadaRadio = element(by.id("address_country_canada"));
  climbingChecbox = element(by.id("address_interest_climb"));
  dancingCheckbox = element(by.id("address_interest_dance"));
  readingCheckbox = element(by.id("address_interest_read"));

  // Buttons
  addressColorButton = element(by.id("address_color"));
  pictureBrowseButton = element(by.id("address_picture"));
  createAddressButton = element(by.name("commit"));
  listButton = element(by.linkText("List"));

  // Dropdowns
  stateDropdown = element(by.id("address_state"));


  // Labels & Text & Links
  addressNoticeLabel = element(by.css("[data-test='notice']"));
}
