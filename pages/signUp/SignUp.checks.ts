import { SignUpSelectors, CommonSelectors, ElementFinder } from ".";

/**
 * @classdesc Sign Up page checks.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class SignUpChecks {
    protected selectors = new SignUpSelectors();
    protected commonSelectors = new CommonSelectors();
   
    async isSignUpSuccessful(email) {
       await expect(await this.commonSelectors.currentUserLabel.getText()).toContain(email);
   }
}