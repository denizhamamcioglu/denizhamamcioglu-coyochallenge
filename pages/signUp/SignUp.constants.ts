/**
 * @classdesc Sign Up page constants.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class SignUpConstants {
  static get content() {
    return {
      URL: "http://a.testaddressbook.com/sign_up", // TODO: make this dynamic according to the browser.params.environment parameter
      pageTitle: "Address Book - Sign Up"
    };
  }
}
