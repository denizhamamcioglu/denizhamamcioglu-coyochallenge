import { JSONParser, browser,  Key } from ".";
import { SignUpSelectors } from "./SignUp.selectors";
import { SignUpConstants } from "./SignUp.constants";
/**
 * @classdesc Sign Up page actions.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */
export class SignUpActions {
    protected selectors = new SignUpSelectors();
    
    // Field actions
   async enterEmail(email: string) {
       await this.selectors.emailField.sendKeys(email);
   }

   async enterPassword(pass: string) {
       await this.selectors.passwordField.sendKeys(pass);
   }

    // Clicks
    async clickSignUp() {
        await this.selectors.signupButton.click();
    }

    async clickSignIn() {
        await this.selectors.signInButton.click();
    }
    
    // Helpers
    async signUp(email: string, password: string) {
        await this.enterEmail(email);
        await this.enterPassword(password);
        await this.clickSignUp();
    }

    async navigate() {
        await browser.get(SignUpConstants.content.URL);
    }
}