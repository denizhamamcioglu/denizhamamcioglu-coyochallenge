import { by, element } from ".";

/**
 * @classdesc Sign Up page selectors.
 * @author Deniz Hamamcioglu - denizhamamcioglu@gmail.com
 */

export class SignUpSelectors {
  // Input Fields
  emailField = element(by.id("user_email"));
  passwordField = element(by.id("user_password"));

  // Checkboxes

  // Buttons
  signupButton = element(by.css("[value='Sign up']"));
  signInButton = element(by.linkText("Sign in"));

  // Dropdowns

  // Labels & Text & Links
  
}
