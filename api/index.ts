export { get, put, post, delete } from "request-promise-native";
export const axios = require("axios");
export { DataGenerator } from "../helpers/DataGenerator";
export { JSONParser } from "../helpers/JSONParser";