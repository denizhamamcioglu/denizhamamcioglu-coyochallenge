import { browser, SignUpActions, SignUpChecks, JSONParser, UserData } from "."

const signUpActions = new SignUpActions();
let credentialInformation = JSONParser.parseJson("configuration\\data\\Credentials.json");
let signUpChecks = new SignUpChecks();

describe('Registration Tests: ', () => {
    beforeAll(async () => {
        browser.ignoreSynchronization = true;
        await signUpActions.navigate();
    });

    it('Should successfully register a new account', async () => {
        const email = UserData.validUser.email.toLowerCase();
        const password = UserData.validUser.password;
        await signUpActions.signUp(email, password);
        await signUpChecks.isSignUpSuccessful(email);
    });
})