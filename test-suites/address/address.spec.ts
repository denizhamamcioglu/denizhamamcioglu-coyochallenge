import { browser, SignUpActions, SignUpChecks, CommonActions, UserData, AddressActions, AddressChecks } from "."

const signUpActions = new SignUpActions();
const signUpChecks = new SignUpChecks();
const commonActions = new CommonActions();
const addressActions = new AddressActions();
const addressChecks = new AddressChecks();

describe('Address Tests: ', () => {
    beforeAll(async () => {
        browser.ignoreSynchronization = true;
        await signUpActions.navigate();
        const email = UserData.validUser.email.toLowerCase();
        const password = UserData.validUser.password;
        await signUpActions.signUp(email, password);
        await signUpChecks.isSignUpSuccessful(email);
    });

    it('Should successfully add a new address', async () => {
        await commonActions.clickAddresses();
        await addressActions.clickNewAddress();
        await addressActions.createNewAddress();
        await addressChecks.isAddressCreated();
    });
})